/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

@ManagedBean
@RequestScoped
public class RegistrasiForm {
   
    private String emailID;
   
    private String password;
    private String confirmPassword;
    
    private String FinalPassword;
    /** Creates a new instance of RegistrationForm */
    public RegistrasiForm() {
    }

    
    /**
     * @return the emailID
     */
    public String getEmailID() {
        return emailID;
    }

    /**
     * @param emailID the emailID to set
     */
    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

 
     /**
     * @return the FinalPassword
     */
    public String getFinalPassword() {
        return FinalPassword;
    }

    /**
     * @param FinalPassword the FinalPassword to set
     */
    public void setFinalPassword(String FinalPassword) {
        this.FinalPassword = FinalPassword;
    }
   public void validateEmail(FacesContext fc, UIComponent c, Object value)throws ValidatorException
    {
        String email= (String)value;
        Pattern mask = null;
        mask = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher matcher = mask.matcher(email);
        if (!matcher.matches())
         {
           FacesMessage message = new FacesMessage();
           message.setDetail("Invalid e-mail ID");
           message.setSummary("Invalid e-mail ID");
           throw new ValidatorException(message);
        }
    }
   
   public void validateName(FacesContext fc, UIComponent c, Object value) 
   {
       if(((String)value).contains("!")||((String)value).contains("@")||((String)value).contains("#")||((String)value).contains("$")||((String)value).contains("%")||((String)value).contains("&")||((String)value).contains("*"))
           throw new ValidatorException(new FacesMessage("Name cannot containt special characters"));
   }
    public void validateFinalPassword(FacesContext fc, UIComponent c, Object value)throws ValidatorException
    {
        setFinalPassword((String) value);
    }
    public void validateCPassword(FacesContext fc, UIComponent c, Object value)throws ValidatorException
    {
        String cPassword= (String)value;
        RegistrasiForm rf=new RegistrasiForm();
        System.out.println(getFinalPassword());
        if (cPassword.compareTo(getFinalPassword())!=0 )
         {
           FacesMessage message = new FacesMessage();
           message.setSummary("Password does not match");
           throw new ValidatorException(message);
        }
    }
}
